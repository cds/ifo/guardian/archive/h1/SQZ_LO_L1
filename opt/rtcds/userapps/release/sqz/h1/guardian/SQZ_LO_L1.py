# -*- mode: python; tab-width: 4; indent-tabs-mode: nil -*-
#
# $Id: SQZ_LO_L1.py $
# $HeadURL: https://redoubt.ligo-wa.caltech.edu/svn/cds_user_apps/trunk/sqz/h1/guardian/SQZ_LO.py $

import sys
import time
from guardian import GuardState, GuardStateDecorator
import cdsutils as cdu

nominal = 'LOCKED_HD'

######FUNCTIONS######

def LO_LOCKED():
    log('In LO_LOCKED()')
    flag = False
    RFave = cdu.avg(1,'H1:SQZ-HD_DIFF_RF3_DEMOD_RFMON', stddev = True)
    if RFave[0] > -5 and RFave[1] < 0.1:
        flag = True
    return flag


def TTFSS_LOCKED():
    if ezca['SQZ-FIBR_LOCK_STATUS_LOCKED'] == 1:
        return True

def OPO_LOCKED():
    if ezca.read('GRD-SQZ_OPO_L1_STATE_S', as_string=True) == 'LOCKED_CLF_DUAL':
        return True


'''def IMC_LOCKED():
    #log('Checking IMC in IMC_LOCKED()')
    flag = False
    if ezca['GRD-IMC_LOCK_STATE_N'] == 100 or ezca['GRD-IMC_LOCK_STATE_N'] == 70:
        flag = True
    if ezca['GRD-IMC_LOCK_STATE_N'] == 55:
        flag = True  
    return flag'''


def CLF_locked():
    return (ezca['SQZ-VCXO_CONTROLS_DIFFFREQUENCY']<5) and (ezca['SQZ-VCXO_CONTROLS_DIFFFREQUENCY']>-5)


#####################

class INIT(GuardState):
    index = 0

    def main(self):
        return True

        if LO_locked():
            return 'LOCKED'
        else:
            return 'DOWN'


class DOWN(GuardState):
    index = 1
    goto = True

    def main(self):
        ezca['SYS-MOTION_C_BDIV_C_CLOSE'] = 1
        #ezca['SQZ-HD_FLIPPER_CONTROL'] = 0 #close HD flipper
        ezca['SQZ-LO_SERVO_COMBOOST'] = 0
        ezca['SQZ-LO_SERVO_IN2EN'] = 0
        ezca['SQZ-LO_SERVO_IN1EN'] = 0
        ezca['SQZ-LO_SERVO_SLOWBOOST'] = 0
        ezca['SQZ-LO_SERVO_SLOWCOMP'] = 0
        ezca['SQZ-LO_SERVO_SLOWFILTER'] = 0
        ezca['SQZ-LO_SERVO_FASTEN'] = 1
        ezca['SQZ-LO_SERVO_SLOWOPT'] = 1
        ezca['SQZ-VCO_CONTROLS_EXTERNAL'] =1
        ezca['SQZ-VCO_CONTROLS_ENABLE'] = 1
        return True

    def run(self):
        return True

class IDLE(GuardState):
    index = 3
    request = False

    def run(self):
        if not TTFSS_LOCKED():
            notify('TTFSS Unlocked')
            return False

        if not OPO_LOCKED():
            notify('OPO Unlocked')
            return False 

        if ezca['SQZ-HD_DIFF_RF3_DEMOD_RFMON'] < -30:
           log('Check 3MHz RF level')
           return False

        return True

#    def run(self):
#        if ezca['SQZ-HD_DIFF_RF3_DEMOD_RFMON'] < -30:
#            log('Check 3MHz RF level')
#        if ezca['GRD-SQZ_PLL_STATE_N'] != 1:
#            log('PLL not DOWN')
#        if abs(ezca['SQZ-LO_SERVO_FASTMON']) == 10:
#            log('LO rails')
#            ezca['SQZ-LO_SERVO_COMBOOST']=0
#        return (ezca['SQZ-HD_DIFF_RF3_DEMOD_RFMON'] > -30) and (ezca['GRD-SQZ_PLL_STATE_N'] == 1) and (abs(ezca['SQZ-LO_SERVO_FASTMON']) < 10)


class ADJUST_FREQUENCY(GuardState):
    index = 4
    request = True


    def run(self):
        BEATNOTEave = cdu.avg(1,'H1:SQZ-FREQ_SQZBEATVSDOUBLEPSLVCO', stddev = True)
        if abs(BEATNOTEave[0]) > 200000:
            ezca['SQZ-VCO_CONTROLS_EXTERNAL'] =1
            ezca['SQZ-VCO_CONTROLS_ENABLE'] = 1            
#            ezca['SQZ-VCO_TUNEOFS'] = -1 #this value has been working fine. Unless something changed. Write a loop later maybe.
            notify('Check 3MHz beat note, adjust 79.4MHz VCO TUNEOFS to optimize.')
        else:
            return True
            

'''
        self.delta_t = 1/16.
        self.ugf = 0.3
        self.cal = 7e-8  #Volt per Hz OPO PZT
        #self.counter = 0
'''
'''
        servo_gain = -2.1e-8 
        control_chan = 'SQZ-OPO_PZT_1_OFFSET'
        readback_chan = 'SQZ-FREQ_SQZBEATVSDOUBLEPSLVCO'
        self.servo = cdu.Servo(ezca, control_chan, readback=readback_chan,
                               gain=servo_gain)
'''

'''   def run(self):
        #if not OPO_LOCKED():
        #    return 'IDLE'
        if not IMC_LOCKED():
            return 'IDLE'
        
        #self.counter += 1
        #log('Counter = {}'.format(self.counter))
        self.servo.step() '''
'''
        err = ezca['SQZ-FREQ_SQZBEATVSDOUBLEPSLVCO'] #ezca['SQZ-FREQ_SQZBEAT'] - 2*ezca['SQZ-FREQ_PSLVCO']
        self.cntrl = self.delta_t*self.ugf*err
        log('err = {}'.format(err))
        ezca['SQZ-OPO_PZT_1_OFFSET'] += self.cal*self.cntrl'''
'''
        if abs(ezca['SQZ-FREQ_SQZBEATVSDOUBLEPSLVCO']) < 50000: 
            return True
'''

   
################################################################################################
#LOCKING 3MHz with Homodyne
################################################################################################

class LOCKING_HD(GuardState):
    index = 5
    request = False

    def main(self):
        log('locking LO Homodyne')
        ezca['SQZ-VCO_CONTROLS_ENABLE'] = 0
        ezca['SQZ-HD_FLIPPER_CONTROL'] = 1
        ezca['SYS-MOTION_C_BDIV_C_CLOSE'] = 1
        #ezca['SQZ-HD_FLIPPER_CONTROL'] = 0 #close HD flipper
        ezca['SQZ-LO_SERVO_COMBOOST'] = 0
        ezca['SQZ-LO_SERVO_IN2EN'] = 0
        ezca['SQZ-LO_SERVO_IN1EN'] = 0
        ezca['SQZ-LO_SERVO_SLOWBOOST'] = 0
        ezca['SQZ-LO_SERVO_SLOWCOMP'] = 0
        ezca['SQZ-LO_SERVO_SLOWFILTER'] = 0
        ezca['SQZ-LO_SERVO_FASTEN'] = 1
        ezca['SQZ-LO_SERVO_SLOWOPT'] = 1
            
    def run(self):

        if not TTFSS_LOCKED():
            return 'IDLE'
        if ezca.read('GRD-SQZ_CLF_L1_STATE_S', as_string=True) != 'LOCKED':
            log('CLF not locked')  


        if abs(ezca['SQZ-FREQ_SQZBEATVSDOUBLEPSLVCO']) < 100000: 
            ezca['SQZ-VCO_CONTROLS_ENABLE'] = 0
            ezca['SQZ-LO_SERVO_IN2EN'] = 1 
            time.sleep(1)

            #RFave = cdu.avg(1,'H1:SQZ-HD_DIFF_RF3_DEMOD_RFMON', stddev = True)
            #if loop closed and stdev OK, move on
            if abs(ezca['SQZ-LO_SERVO_SLOWMON']) > 9:
                notify('LO is not really locked, relocking')
            else:
                return True
        else:
            notify('Low 3MHz signal, adjusting VCO frequency')
            ezca['SQZ-VCO_CONTROLS_ENABLE'] = 1
            

#ezca.read('GRD-SQZ_LOCK_STATE_S', as_string=True) == 'LOCKED'

class LOCKED_HD(GuardState):
    index = 10

    def main(self):
        RFave = cdu.avg(1,'H1:SQZ-HD_DIFF_RF3_DEMOD_RFMON', stddev = True)
        if RFave[1] < 0.05:
            log('LO LOCKED')
            notify('LO LOCKED')
            ezca['SQZ-LO_SERVO_COMBOOST']=2

    def run(self):

        RFave = cdu.avg(1,'H1:SQZ-HD_DIFF_RF3_DEMOD_RFMON', stddev = True)
        #log('RF3 average 1s = {}'.format(RFave[0]))
        #log('RF3 std 1s = {}'.format(RFave[1]))
 

        if not TTFSS_LOCKED():
           log('TTFSS not locked')
           notify('TTFSS not locked')
           return 'LOCKING_HD'

        if not OPO_LOCKED():
           log('OPO not locked')
           notify('OPO not locked')
           return 'LOCKING_HD'

        if RFave[1] > 0.1:
            log('LO Not locked')
            return 'LOCKING_HD'

        return True


#Make SQZ_LOCK check for status and tell this guardian to go places


################################################################################################
#LOCKING 3MHz with OMC
################################################################################################


class LOCKING_OMC(GuardState):
    index = 15
    request = False

    def main(self):
        log('locking LO with OMC')
        ezca['SQZ-VCO_CONTROLS_ENABLE'] = 0

    def run(self):

        if not TTFSS_LOCKED():
            return 'IDLE'
        if ezca.read('GRD-SQZ_CLF_L1_STATE_S', as_string=True) != 'LOCKED':
            log('CLF not locked')  

        if abs(ezca['SQZ-FREQ_SQZBEATVSDOUBLEPSLVCO']) < 100000: 
            ezca['SYS-MOTION_C_BDIV_C_OPEN'] = 1 #open beam diverter
            time.sleep(1)
        else:
            notify('Check SQZBEATVSDOUBLEPSLVCO, adjusting VCO frequency')
            ezca['SQZ-VCO_CONTROLS_ENABLE'] = 1


        if ezca['SYS-MOTION_C_BDIV_C_POSITION'] == 0: # if the beam diverter is open
            if ezca['SQZ-OMC_TRANS_RF3_DEMOD_RFMON'] >= -25:
                log('Got 3MHz Beat Note, engaging LO loop')
                notify('Got 3MHz Beat Note, engaging LO loop')
                ezca['SQZ-LO_SERVO_IN1EN'] = 1
                return True
            else:
                log('Beam diverter is open but no 3MHz on OMC')

                #ezca['SQZ-LO_SERVO_IN1EN'] = 1
                #time.sleep(0.1)
                #if LO_LOCKED(): 
                #    return True
                #else:
                #    notify('beam diverter is open but no 3MHz on OMC')


class LOCKED_OMC(GuardState):
    index = 20

    def main(self):
        #look at RF3 rms 
        RFave = cdu.avg(1,'H1:SQZ-OMC_TRANS_RF3_DEMOD_RFMON', stddev = True)
        #check good rms value 0.05 works for HD
        if RFave[1] < 1:
            log('LO LOCKED WITH OMC')
            notify('LO LOCKED WITH OMC')
            ezca['SQZ-LO_SERVO_COMBOOST']=2
            
            return True


    def run(self):
        #self.servo.step() 
        #Look at transfer function, make sure the gain is good before turning on boosts
        #ezca['SQZ-LO_SERVO_COMBOOST']=1
        #ezca['SQZ-LO_SERVO_SLOWBOOST']=1
        RFave = cdu.avg(1,'H1:SQZ-OMC_TRANS_RF3_DEMOD_RFMON', stddev = True)

        if not TTFSS_LOCKED():
           log('TTFSS not locked')
           notify('TTFSS not locked')
           return 'LOCKING_OMC'

        if not OPO_LOCKED():
           log('OPO not locked')
           notify('OPO not locked')
           return 'LOCKING_OMC'
        #double check this
        if RFave[1] > 2:
            log('LO Not locked')
            return 'LOCKING_OMC'

        return True




##################################################

edges = [
    ('INIT', 'DOWN'),
    ('IDLE', 'DOWN'),
    ('DOWN', 'ADJUST_FREQUENCY'),
    ('ADJUST_FREQUENCY','LOCKING_HD'),
    ('IDLE','LOCKING_HD'),
    ('LOCKING_HD', 'LOCKED_HD'),
    ('LOCKED_HD', 'DOWN'),
    ('ADJUST_FREQUENCY','LOCKING_OMC'),
    ('IDLE','LOCKING_OMC'),
    ('LOCKING_OMC', 'LOCKED_OMC'),
    ('LOCKED_OMC', 'DOWN')]


